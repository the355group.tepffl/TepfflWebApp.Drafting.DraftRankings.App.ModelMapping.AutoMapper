﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TepfflWebApp.Drafting.DraftRankings.App.ModelMapping.AutoMapper
{
    public class Startup
    {
        /// <summary>
        /// Returns a list of types that are derived from AutoMapper.Profile.
        /// 
        /// If no matches are found, it will return an empty list.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<System.Type> GetProfileTypes()
        {
            try
            {
                var assembly = typeof(Startup).Assembly; // Going this route per SonarQube suggestion at https://rules.sonarsource.com/csharp/RSPEC-3902
                return assembly.GetTypes()
                    .Where(
                        x => x.GetTypeInfo().IsClass
                            && x.IsAssignableFrom(x)
                            && x.GetTypeInfo().BaseType == typeof(Profile)
                    ).ToList<System.Type>() ?? new List<System.Type>();
            }
            catch (ReflectionTypeLoadException ex)
            {
                throw ex;
            }
        }
    }
}
