﻿using AutoMapper;
using TepfflWebApp.Domain.Core.Models;
using TepfflWebApp.Drafting.DraftRankings.App.ApiModels.V3;
using TepfflWebApp.Drafting.DraftRankings.Core.Models;

namespace TepfflWebApp.Drafting.DraftRankings.App.ModelMapping.AutoMapper.Profiles.DomainToApiModels
{
    public class V3ApiModelsProfile : Profile
    {
        public V3ApiModelsProfile()
        {            
            CreateMap<PlayerInstance, DraftRankingPlayer>()
                .ForMember(dest => dest.DisplayedPlayingStatus, opt => opt.MapFrom(src => src.PlayingStatus))
                .ForMember(dest => dest.DisplayedName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.PositionAbbreviation, opt => opt.MapFrom(src => src.Position.ShortName))
                .ForMember(dest => dest.PositionId, opt => opt.MapFrom(src => src.Position.Id))
                .ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.Position.Name))
                .ForMember(dest => dest.SeasonStats, opt => opt.MapFrom(src => src.FantasyStats))
                .ForMember(dest => dest.Tags, opt => opt.Ignore())
                .AfterMap((src, dest, context) =>
                {

                })
            ;            

            CreateMap<Core.Models.DraftRankingPod, ApiModels.V3.DraftRankingPod>()
                // .ForMember(dest => dest.Players, opt => opt.Ignore())
            ;

            CreateMap<RankingsTag, RankingTag>()                
            ;

            CreateMap<RankingsTaggedPlayer, DraftRankingPlayer>()
                .IncludeBase<PlayerInstance, DraftRankingPlayer>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.Tags))
            ;
        }
    }
}
