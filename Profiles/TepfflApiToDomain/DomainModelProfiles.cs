﻿using AutoMapper;
using System.Linq;
using TepfflWebApp.Domain.Core.Models;
using TepfflWebApp.Drafting.DraftRankings.Core.Models;
using The355Group.tepffl.DomainLogicTier.DataTransferObjects;

namespace TepfflWebApp.Drafting.DraftRankings.App.ModelMapping.AutoMapper.Profiles.TepfflApiToDomain
{
    class DomainModelProfiles : Profile
    {
        public DomainModelProfiles()
        {
            CreateMap<DraftRankingPodDTO, DraftRankingPod>()
                .ForMember(dest => dest.SeasonId, opt => opt.MapFrom(
                    (src, dest, destMember, context) => 
                        src.Team == null || src.Team.Season == null 
                        ? context != null && context.Options.Items != null && context.Options.Items.ContainsKey("Season") 
                            ? ((Season)(context.Options.Items["Season"])).SeasonId
                            : 0 
                        : src.Team.Season.SeasonId)
                )
                .ForMember(dest => dest.SeasonYear, opt => opt.MapFrom(
                    (src, dest, destMember, context) =>
                        src.Team == null || src.Team.Season == null
                        ? context != null && context.Options.Items != null && context.Options.Items.ContainsKey("Season")
                            ? ((Season)(context.Options.Items["Season"])).SeasonYear
                            : 0
                        : src.Team.Season.SeasonYear)
                )
                .ForMember(dest => dest.PositionView, opt => opt.MapFrom(src => src.PositionView))                
            ;

            CreateMap<DraftRankingPositionViewDTO, DraftRankingPositionView>()
                .ForMember(dest => dest.PositionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.PositionShortName, opt => opt.MapFrom(src => src.ShortName))
                .ForMember(dest => dest.SortOrder, opt => opt.MapFrom(src => src.SortOrder))
            ;

            CreateMap<KeeperRankingSourceDTO, RankingsSource>()
                .ForMember(dest => dest.SourceId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.SourceName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.SourceUrl, opt => opt.MapFrom(src => src.Url))
            ;

            CreateMap<TeamDraftRankingsTagDTO, RankingsTag>()
                .ForMember(dest => dest.ColorName, opt => opt.MapFrom(src => RankingsTagColors.Options.FirstOrDefault(t => t.Value == src.Color).Key))
                .ForMember(dest => dest.SortWeight, opt => opt.Ignore())
                .ReverseMap()
            ;

            CreateMap<PlayerInstanceDTO, RankingsTaggedPlayer>()
                .IncludeBase<PlayerInstanceDTO, PlayerInstance>()
                .ForMember(dest => dest.Tags, opt => opt.Ignore())
                .ForMember(dest => dest.OverallRank, opt => opt.MapFrom(src => src.SeasonStats == null ? 0 : src.SeasonStats.OverallRank))
                .ForMember(dest => dest.OverallDraftRank, opt => opt.Ignore())
                .ForMember(dest => dest.PositionDraftRank, opt => opt.Ignore())
                .ReverseMap()
            ;

            CreateMap<PlayerInstance, RankingsTaggedPlayer>()
                .ForMember(dest => dest.Tags, opt => opt.Ignore())
                .ForMember(dest => dest.OverallRank, opt => opt.MapFrom(src => src.FantasyStats == null ? 0 : src.FantasyStats.OverallRank))
                .ForMember(dest => dest.OverallDraftRank, opt => opt.Ignore())
                .ForMember(dest => dest.PositionDraftRank, opt => opt.Ignore())
            ;

            CreateMap<DraftRankingsPlayerDTO, RankingsTaggedPlayer>()
                .IncludeBase<PlayerInstanceDTO, PlayerInstance>()                
                .ForMember(dest => dest.OverallRank, opt => opt.Ignore())
                .ForMember(dest => dest.OverallDraftRank, opt => opt.MapFrom(src => src.OverallDraftRank))
                .ForMember(dest => dest.PositionDraftRank, opt => opt.MapFrom(src => src.PositionDraftRank))
                .ReverseMap()
            ;

            
        }
    }
}
