﻿using AutoMapper;
using TepfflWebApp.Drafting.DraftRankings.App.ViewModels.Shared;
using TepfflWebApp.Drafting.DraftRankings.Core.Models;

namespace TepfflWebApp.Drafting.DraftRankings.App.ModelMapping.AutoMapper.Profiles.DomainToViewModels
{
    class SharedViewModelProfiles : Profile
    {
        public SharedViewModelProfiles()
        {
            CreateMap<RankingsSource, RankingsSourceVM>()
            ;

            CreateMap<DraftRankingPositionView, DraftRankingPositionViewVM>()
            ;            
        }
    }
}
