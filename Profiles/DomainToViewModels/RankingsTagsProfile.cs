﻿using AutoMapper;
using TepfflWebApp.Domain.App.ViewModels.Shared;
using TepfflWebApp.Domain.Core.Models;
using TepfflWebApp.Drafting.DraftRankings.App.ViewModels.Shared;
using TepfflWebApp.Drafting.DraftRankings.Core.Models;

namespace TepfflWebApp.Drafting.DraftRankings.App.ModelMapping.AutoMapper.Profiles.DomainToViewModels
{
    class RankingsTagsProfile : Profile
    {
        public RankingsTagsProfile()
        {
            CreateMap<RankingsTag, TagVM>()
                .ForMember(dest => dest.SeasonYear, opt => opt.MapFrom(src => src.Season == null ? 0 : src.Season.SeasonYear))
            ;

            CreateMap<RankingsTaggedPlayer, DraftRankingsPlayerVM>()
                .IncludeBase<PlayerInstance, PlayerInstanceVM>()
            ;
        }
    }
}
